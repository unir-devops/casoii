terraform {
    required_providers {
      azurerm = {
         source = "hashicorp/azurerm"
         version = "=2.46.1"
      }
    }
}

# Create a resource group for the new VM
resource "azurerm_resource_group" "rg" {
  name     = "kubernetes_rg"
  location = var.location

  tags = {
      environment = "CP2"
  }
}

# Create a storage account
resource "azurerm_storage_account" "stAccount" {
    name = var.storage_account
    resource_group_name = azurerm_resource_group.rg.name
    location = azurerm_resource_group.rg.location
    account_tier = "Standard"
    account_replication_type = "LRS"

    tags = {
        environment = "CP2"
    }
}