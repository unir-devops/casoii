# Security Group
# Allow SSH port for the remote administration
resource "azurerm_network_security_group" "mySecGroup" {
  count               = length(var.vms)
  name                = "sshtraffic${count.index}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "SSH"
    description                = "Allow SSH access"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
      environment = "CP2"
  }
}

# Linked the security group to the NIC
resource "azurerm_network_interface_security_group_association" "mySecGroupAssociation1" {
    count                     = length(var.vms)
    network_interface_id      = azurerm_network_interface.myNic[count.index].id
    network_security_group_id = azurerm_network_security_group.mySecGroup[count.index].id
}