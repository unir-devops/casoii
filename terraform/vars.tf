# Variable to set size of virtual machine
variable "vm_size"{
    type        = string
    description = "Size of virtual machine"
    default     = "Standard_F2s_v2" # 4 GB, 2 CPU
#    default     = "Standard_D1_v2" # 3.5 GB, 1 CPU
#    default     = "Standard_B1ms" # 2 GB, 1 CPU
}

variable "vms"{
    description = "Virtual machines to create"
    type = list(string)
    default = ["master", "worker01"]
#    default = ["master", "worker01", "worker02", "nfs"]
}