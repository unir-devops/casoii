# Variable to set location of Azure region to deploy the infrastructure
variable "location"{
    type        = string
    description = "Azure region to deploy the infrastructure"
    default     = "West Europe"
}

# Variable to set the storage account name
variable "storage_account" {
  type = string
  description = "Variable to set the storage account name"
  default = "28a0fb87350e4b67b02a313"
}

# Variable to set the path of the publick key
variable "public_key_path" {
  type = string
  description = "Variable to set the path of the publick key"
  default = "~/.ssh/id_rsa.pub"
}

# Variable to set the user name to deploy kubernetes
variable "ssh_user" {
  type = string
  description = "Variable to set the user name to deploy kubernetes"
  default = "adminusername"
}