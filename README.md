Este informe sirve como guía para desplegar una infraestructura de máquinas en Azure mediante terraform. Las máquinas son un servidor master y un servidor worker que formarán un clúster de Kubernetes en el que se va a desplegar una aplicación que expondrá una IP y un puerto para que sea accesible desde fuera.

El servidor máster también hará de servidor NFS para compartir un volumen con la aplicación  desplegada.

Para la realización de este proyecto se usarán dos servidores GNU-Linux Debian.
Desde otra máquina Linux que tiene instalado Ansible, se ejecutarán los comandos para realizar el despliegue.

```bash
mkdir deploy 
cd deploy
```

Y clonamos el repositorio:

```bash
git clone https://gitlab.com/unir-devops/casoii.git
```

Navegamos al directorio que se nos ha creado

```bash
cd casoii
```

## Despliegue

Una vez configurados los ficheros antes descritos,
procedemos al despliegue.

Nos ubicamos en el directorio de terraform

```bash
cd terraform/
```

Y ejecutamos la siguiente serie de comandos

```bash
terraform init
terraform plan
terraform apply
```

Y cuando nos salga lo siguiente, escribimos *yes*

```bash
Do you want to perform these actions? Terraform will perform the actions described above.
Only 'yes' will be accepted to approve.
Enter a value: **yes**
```

Si todo ha ido bien, al final veremos algo parecido a lo siguiente:

```bash
Apply complete! Resources: 14 added, 0 changed, 0 destroyed.
```

En el portal de azure, en la sección de virtual machines, deberáin verse los servidores:

**Ansible: Despliegue de Kubernetes**

En la consola del portal de azure, nos quedamos con las IPs públicas de los servidores que se han desplegado:

En la máquina desde la que hemos desplegado el plan de terraform, navegamos hacia la ruta donde están los ficheros de ansible.

```bash
cd ../ansible
```

Y editamos los siguientes ficheros:

```bash
nano inventory.azure
```

Otro fichero a cambiar es el de exports que se ubica en la siguiente ruta:

```bash
nano roles/setup-nfs-server/files/exports
```

Donde pondremos las IPs correspondiente al servidor master como al worker

**Ejecución de scripts para despliegue de playbooks**

Damos permisos de ejecución al script **deploy.sh**

```bash
chmod +x deploy.sh
```

Y corremos el script

```bash
sh deploy.sh
```
