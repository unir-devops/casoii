#!/bin/bash
ansible-playbook -i inventory.azure 01-playbook-update-upgrade-servers.yaml
ansible-playbook -i inventory.azure 02-playbook-install-common-services.yaml
ansible-playbook -i inventory.azure 03-playbook-setup-nfs-server.yaml
ansible-playbook -i inventory.azure 04-playbook-common-tasks-masters-workers.yaml
ansible-playbook -i inventory.azure 05-playbook-config-k8s-master.yaml
ansible-playbook -i inventory.azure 06-playbook-config-k8s-workers.yaml
ansible-playbook -i inventory.azure 07-deploy-pihole.yaml